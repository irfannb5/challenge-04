class App {
  constructor() {
    this.clearButton = document.getElementById("clear-btn");
    this.loadButton = document.getElementById("load-btn");
    this.carContainerElement = document.getElementById("cars-container");
    const urlSearchParams = new URLSearchParams(window.location.search);
    const params = Object.fromEntries(urlSearchParams.entries());
  }

  async init() {
    await this.load();

    // Register click listener
    this.clearButton.onclick = this.clear;
    this.loadButton.onclick = this.run;
  }

  run = () => {
    let tanggalBook = new Date(params.tanggal);
    let waktuBooking = new Date(
      params.tanggal + " " + params.waktuJemput
    ).getHours();
    console.log("tanggal jam Book: " + waktuBooking);
    let count = 0;
    Car.list.forEach((car) => {
      let batasTersedia = new Date(car.availableAt);
      let availat = batasTersedia.getHours();
      console.log("mobil time: " + availat);
      if (waktuBooking < availat) {
        console.log(waktuBooking + " Kurang Dari Batas " + availat);
      }
      if (
        car.capacity == params.jumlahPenumpang &&
        tanggalBook.toDateString() == batasTersedia.toDateString() &&
        waktuBooking <= availat &&
        car.driverType == params.tipeDriver
      ) {
        count++;
        const node = document.createElement("span");
        node.classList.add("col");
        node.innerHTML = car.render();
        this.carContainerElement.appendChild(node);
      }
    });
    if (count == 0) {
      const node = document.createElement("span");
      node.classList.add("col");
      node.innerHTML = Car.renderZero();
      this.carContainerElement.appendChild(node);
    }
  };

  async load() {
    const cars = await Binar.listCars();
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainerElement.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainerElement.firstElementChild;
    }
  };
}
